<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output method="xml" indent="yes"/>

    <xsl:template match="/">
        <start>
            <xsl:apply-templates/>
        </start>

    </xsl:template>

    <xsl:template match="units">
        <xsl:element name="units">
            <xsl:apply-templates/>
        </xsl:element>

    </xsl:template>

    <xsl:template match="translationUnit">
        <xsl:element name="translationUnit">
            <xsl:call-template name="transUnitRecursive"/>
        </xsl:element>
    </xsl:template>

    <xsl:template name="ClassDecl">
        <ClassDecl>
            <xsl:attribute name="lineOfCode">
                <xsl:value-of select="@lineOfCode"/>
            </xsl:attribute>
            <xsl:attribute name="transUnit">
                <xsl:value-of select="@transUnit"/>
            </xsl:attribute>
            <name>
                <xsl:value-of select="./name"/>
            </name>

            <visibility>
                <xsl:value-of select="./visibility"/>
            </visibility>

            <modifiers class="java.util.LinkedList"/>
            <annotations class="java.util.LinkedList"/>
            <templates class="java.util.Collections$EmptyList"/>
            <xsl:choose>
                <xsl:when test="./inherits/type[@class='edu.diploma.metamodel.types.ClassType']">
                    <inherits>
                    <type class="ClassType">
                        <name><xsl:value-of select="./inherits/type/name"/></name>
                        <templates class="java.util.Collections$EmptyList"/>
                    </type>
                    </inherits>
                </xsl:when>
                <xsl:otherwise>
                    <inherits/>
                </xsl:otherwise>
            </xsl:choose>
            <body>
                <name><xsl:value-of select="./body/name"/></name>
                <visibility><xsl:value-of select="./body/visibility"/></visibility>
                <modifiers class="java.util.LinkedList"/>
                <annotations class="java.util.LinkedList"/>
                <decls>
                    <xsl:apply-templates select="./body/decls"/>
                </decls>
            </body>
        </ClassDecl>
    </xsl:template>

    <xsl:template name="transUnitRecursive">
        <imports>
            <xsl:for-each select="./imports/import">
            <import>
                <name><xsl:value-of select="./name"/></name>
                <isStatic><xsl:value-of select="./isStatic"/></isStatic>
                <isWildcard><xsl:value-of select="./isStatic"/></isWildcard>
            </import>
            </xsl:for-each>
        </imports>
        <types>
        <xsl:for-each
                select="./types/declaration[@class='edu.diploma.metamodel.declarations.ClassDecl']">
            <xsl:call-template name="ClassDecl"/>
        </xsl:for-each>
        </types>
    </xsl:template>

    <xsl:template name="Declaration">
        <!-- <DECL>DECL</DECL> -->
        <!-- <DECLCLASS><xsl:value-of select="./@class"/></DECLCLASS> -->
        <xsl:if test="./@class='edu.diploma.metamodel.declarations.FunctionDecl'">
            <xsl:call-template name="FunctionDecl"/>
        </xsl:if>
        <xsl:if test="./@class='edu.diploma.metamodel.declarations.ClassDecl'">
            <xsl:call-template name="ClassDecl"/>
        </xsl:if>
        <xsl:if test="./@class='edu.diploma.metamodel.declarations.DeclBody'">
            <xsl:call-template name="DeclBody"/>
        </xsl:if>
        <xsl:if test="./@class='edu.diploma.metamodel.declarations.ParameterDecl'">
            <xsl:call-template name="ParameterDecl"/>
        </xsl:if>
        <xsl:if test="./@class='edu.diploma.metamodel.declarations.VariableDecl'">
            <xsl:call-template name="VariableDecl"/>
        </xsl:if>
        <!-- TODO: other types support -->
    </xsl:template>

    <xsl:template name="VariableDecl">
        <VariableDecl>
            <name><xsl:value-of select="./name"/></name>
            <visibility><xsl:value-of select="./visibility"/></visibility>
            <modifiers>
                <xsl:for-each select="./modifiers">
                    <string>
                        <xsl:value-of select="./string"/>
                    </string>
                </xsl:for-each>
            </modifiers>
            <annotations class="java.util.LinkedList"/>
            <xsl:if test="./type[@class='edu.diploma.metamodel.types.ClassType']">
                <type class="ClassType">
                    <name><xsl:value-of select="./type/name"/></name>
                    <templates class="java.util.Collections$EmptyList"/>
                </type>
            </xsl:if>
            <xsl:if test="./type[@class='edu.diploma.metamodel.types.PrimitiveType']">
                <type class="PrimitiveType">
                    <name><xsl:value-of select="./type/name"/></name>
                </type>
            </xsl:if>
            <xsl:if test="./type[@class='edu.diploma.metamodel.types.ArrayType']">
                <type class="ArrayType">
                    <xsl:if test="./type/type[@class='edu.diploma.metamodel.types.ClassType']">
                        <type class="ClassType">
                            <name><xsl:value-of select="./type/type/name"/></name>
                            <templates class="java.util.Collections$EmptyList"/>
                        </type>
                    </xsl:if>
                    <xsl:if test="./type/type[@class='edu.diploma.metamodel.types.PrimitiveType']">
                        <type class="PrimitiveType">
                            <name><xsl:value-of select="./type/name"/></name>
                        </type>
                    </xsl:if>
                </type>
            </xsl:if>
            <!-- TODO other types support -->

        </VariableDecl>
    </xsl:template>

    <xsl:template name="DeclBody">
        <ClassOrInstanceMemberDecl>
            <name></name>
            <visibility><xsl:value-of select="./visibility"/></visibility>
            <modifiers>
                <xsl:for-each select="./modifiers">
                    <string>
                        <xsl:value-of select="./string"/>
                    </string>
                </xsl:for-each>
            </modifiers>
            <annotations class="java.util.LinkedList"/>
            <decls class="java.util.LinkedList">
                <xsl:for-each select="./decls/declaration">
                    <xsl:call-template name="Declaration"/>
                </xsl:for-each>
            </decls>
        </ClassOrInstanceMemberDecl>
    </xsl:template>

    <xsl:template name="ParameterDecl">
        <ParameterDecl>
            <name><xsl:value-of select="./name"/></name>
            <visibility><xsl:value-of select="./visibility"/></visibility>
            <modifiers>
                <xsl:for-each select="./modifiers">
                    <string>
                        <xsl:value-of select="./string"/>
                    </string>
                </xsl:for-each>
            </modifiers>
            <annotations class="java.util.LinkedList"/>
            <value>
                <name><xsl:value-of select="./value/name"/></name>
                <visibility><xsl:value-of select="./value/visibility"/></visibility>
                <modifiers>
                    <xsl:for-each select="./value/modifiers">
                        <string>
                            <xsl:value-of select="./string"/>
                        </string>
                    </xsl:for-each>
                </modifiers>
                <annotations class="java.util.LinkedList"/>
                <xsl:if test="./value/type[@class='edu.diploma.metamodel.types.ArrayType']" >
                    <type class="ArrayType">
                        <xsl:if test="./value/type/type[@class='edu.diploma.metamodel.types.ClassType']" >
                            <type class="ClassType">
                                <name><xsl:value-of select="./value/type/type/name"/></name>
                            </type>
                        </xsl:if>
                        <!-- TODO: else branches -->
                    </type>
                </xsl:if>
                <xsl:if test="./value/type[@class='edu.diploma.metamodel.types.ClassType']" >

                    <type class="ClassType">
                        <name><xsl:value-of select="./value/type/name"/></name>
                        <templates class="java.util.Collections$EmptyList"/>
                    </type>
                </xsl:if>
                <xsl:if test="./value/type[@class='edu.diploma.metamodel.types.PrimitiveType']">
                    <type class="PrimitiveType">
                        <name><xsl:value-of select="./value/type/name"/></name>
                    </type>

                </xsl:if>
            </value>
            <variadic><xsl:value-of select="./variadic"/></variadic>
        </ParameterDecl>
    </xsl:template>

    <xsl:template name="FunctionDecl">
        <FunctionDecl>
            <name> <xsl:value-of select="./name"/> </name>
            <visibility> <xsl:value-of select="./visibility"/> </visibility>
            <modifiers>
                <xsl:for-each select="./modifiers">
                    <string>
                        <xsl:value-of select="./string"/>
                    </string>
                </xsl:for-each>
            </modifiers>
            <annotations class="java.util.LinkedList"/>
            <xsl:if test="./retType[@class='edu.diploma.metamodel.types.PrimitiveType']">
                <retType class="PrimitiveType">
                    <name><xsl:value-of select="./retType/name"/></name>
                </retType>
            </xsl:if>
            <!-- TODO: else branches (for other return types) !!!  -->
            <exceptions class="java.util.Collections$EmptyList"/>
            <params>
                <xsl:apply-templates select="./params"/>
            </params>
            <body>
                <xsl:apply-templates select="./body"/>
            </body>

        </FunctionDecl>
    </xsl:template>

    <xsl:template match="*/types/declaration/body/decls" name="ClassDeclBodyRecursive">
        <xsl:for-each
             select="./declaration[@class]">
            <xsl:call-template name="Declaration"/>

        </xsl:for-each>
    </xsl:template>

    <xsl:template match="*/declaration/params" name="MethodParams">

        <xsl:for-each
                select="./parameterDecl">
            <xsl:call-template name="ParameterDecl"/>
        </xsl:for-each>

    </xsl:template>

    <xsl:template match="*/declaration/body" name="MethodBody">
        <statements>
            <xsl:for-each
                    select="./statements/statement">
                <!-- <D>STATEMENT</D> -->
                <!-- <F><xsl:value-of select="@class"/></F> -->
                <xsl:call-template name="Statement"/>
            </xsl:for-each>

        </statements>

    </xsl:template>

    <xsl:template name="Statement">

        <xsl:if test="./@class='edu.diploma.metamodel.expressions.UnaryExpression'">
                <xsl:call-template name="UnaryExpression"/>
        </xsl:if>
        <xsl:if test="./@class='edu.diploma.metamodel.expressions.BinaryExpression'">
            <BinaryExpression>BINARY</BinaryExpression>
        </xsl:if>
        <xsl:if test="./@class='edu.diploma.metamodel.expressions.ArrayAccessExpression'">
            <ArrayAccessExpression>ArrayAccessExpression</ArrayAccessExpression>
        </xsl:if>
        <xsl:if test="./@class='edu.diploma.metamodel.expressions.ArrayConstructorCall'">
            <ArrayConstructorCall>ArrayConstructorCall</ArrayConstructorCall>
        </xsl:if>
        <xsl:if test="./@class='edu.diploma.metamodel.expressions.ArrayInitializer'">
            <ArrayInitializer>ArrayInitializer</ArrayInitializer>
        </xsl:if>
        <xsl:if test="./@class='edu.diploma.metamodel.expressions.AssignmentExpression'">
            <xsl:call-template name="AssignmentExpression"/>
        </xsl:if>
        <xsl:if test="./@class='edu.diploma.metamodel.expressions.AttributeAccess'">
            <AttributeAccess>AttributeAccess</AttributeAccess>
        </xsl:if>
        <xsl:if test="./@class='edu.diploma.metamodel.expressions.CastExpression'">
            <CastExpression>CastExpression</CastExpression>
        </xsl:if>
        <xsl:if test="./@class='edu.diploma.metamodel.expressions.ConstructorCall'">
            <ConstructorCall>ConstructorCall</ConstructorCall>
        </xsl:if>
        <xsl:if test="./@class='edu.diploma.metamodel.expressions.FunctionCall'">
            <xsl:call-template name="FunctionCall"/>
        </xsl:if>
        <xsl:if test="./@class='edu.diploma.metamodel.expressions.StaticAttributeAccess'">
            <StaticAttributeAccess>StaticAttributeAccess</StaticAttributeAccess>
        </xsl:if>
        <xsl:if test="./@class='edu.diploma.metamodel.expressions.VariableReference'">
            <VariableReference>VariableReference</VariableReference>
        </xsl:if>
        <!-- TODO: more expressions -->
        <xsl:if test="./@class='edu.diploma.metamodel.statements.IfStatement'">
            <xsl:call-template name="IfStatement"/>
        </xsl:if>
        <xsl:if test="./@class='edu.diploma.metamodel.statements.WhileStatement'">
            <xsl:call-template name="WhileStatement"/>
        </xsl:if>
        <xsl:if test="./@class='edu.diploma.metamodel.statements.ForStatement'">
            <xsl:call-template name="ForStatement"/>
        </xsl:if>
        <xsl:if test="./@class='edu.diploma.metamodel.statements.VariableDeclStatement'">
            <xsl:call-template name="VariableDeclStatement"/>
        </xsl:if>
        <xsl:if test="./@class='edu.diploma.metamodel.statements.BreakStatement'">
            <xsl:call-template name="BreakStatement"/>
        </xsl:if>
        <xsl:if test="./@class='edu.diploma.metamodel.statements.ContinueStatement'">
            <xsl:call-template name="ContinueStatement"/>
        </xsl:if>
        <xsl:if test="./@class='edu.diploma.metamodel.statements.SwitchStatement'">
            <xsl:call-template name="SwitchStatement"/>
        </xsl:if>
        <xsl:if test="./@class='edu.diploma.metamodel.statements.ReturnStatement'">
            <xsl:call-template name="ReturnStatement"/>
        </xsl:if>
        <xsl:if test="./@class='edu.diploma.metamodel.statements.EmptyStatement'">
            <EmptyStatement>EmptyStatement</EmptyStatement>
        </xsl:if>

    </xsl:template>

    <xsl:template match="*/expressions/expression" name="Expression">

        <xsl:if test="./@class='edu.diploma.metamodel.expressions.UnaryExpression'">
            <xsl:call-template name="UnaryExpression"/>
        </xsl:if>
        <xsl:if test="./@class='edu.diploma.metamodel.expressions.BinaryExpression'">
            <BinaryExpression>BINARY</BinaryExpression>
        </xsl:if>
        <xsl:if test="./@class='edu.diploma.metamodel.expressions.ArrayAccessExpression'">
            <ArrayAccessExpression>ArrayAccessExpression</ArrayAccessExpression>
        </xsl:if>
        <xsl:if test="./@class='edu.diploma.metamodel.expressions.ArrayConstructorCall'">
            <ArrayConstructorCall>ArrayConstructorCall</ArrayConstructorCall>
        </xsl:if>
        <xsl:if test="./@class='edu.diploma.metamodel.expressions.ArrayInitializer'">
            <ArrayInitializer>ArrayInitializer</ArrayInitializer>
        </xsl:if>
        <xsl:if test="./@class='edu.diploma.metamodel.expressions.AssignmentExpression'">
            <xsl:call-template name="AssignmentExpression"/>
        </xsl:if>
        <xsl:if test="./@class='edu.diploma.metamodel.expressions.AttributeAccess'">
            <AttributeAccess>AttributeAccess</AttributeAccess>
        </xsl:if>
        <xsl:if test="./@class='edu.diploma.metamodel.expressions.CastExpression'">
            <CastExpression>CastExpression</CastExpression>
        </xsl:if>
        <xsl:if test="./@class='edu.diploma.metamodel.expressions.ConstructorCall'">
            <ConstructorCall>ConstructorCall</ConstructorCall>
        </xsl:if>
        <xsl:if test="./@class='edu.diploma.metamodel.expressions.FunctionCall'">
            <xsl:call-template name="FunctionCall"/>
        </xsl:if>
        <xsl:if test="./@class='edu.diploma.metamodel.expressions.StaticAttributeAccess'">
            <StaticAttributeAccess>StaticAttributeAccess</StaticAttributeAccess>
        </xsl:if>
        <xsl:if test="./@class='edu.diploma.metamodel.expressions.VariableReference'">
            <VariableReference>VariableReference</VariableReference>
        </xsl:if>
    </xsl:template>

    <xsl:template name="ReturnStatement">
        <ReturnStatement>

            <xsl:if test="./expr[@class='edu.diploma.metamodel.literals.BooleanLiteral']">
                <expr class='BooleanLiteral'>
                    <type class="PrimitiveType">
                        <name><xsl:value-of select="./expr/type/name"/></name>
                    </type>
                    <value><xsl:value-of select="./expr/value"/></value>
                </expr>
            </xsl:if>
            <xsl:if test="./expr[@class='edu.diploma.metamodel.literals.IntegerLiteral']">
                <expr class="IntegerLiteral">
                    <type class="PrimitiveType">
                        <name><xsl:value-of select="./expr/type/name"/></name>
                    </type>
                    <value><xsl:value-of select="./expr/value"/></value>
                </expr>
            </xsl:if>
            <xsl:if test="./expr[@class='edu.diploma.metamodel.expressions.VariableReference']">
                <expr class="VariableReference">
                    <type class="PrimitiveType">
                        <name><xsl:value-of select="./expr/type/name"/></name>
                    </type>
                    <name><xsl:value-of select="./expr/name"/></name>
                </expr>
            </xsl:if>

        </ReturnStatement>

    </xsl:template>

    <xsl:template name="ForStatement">
        <ForStatement>
            <xsl:attribute name="lineOfCode">
                <xsl:value-of select="@lineOfCode"/>
            </xsl:attribute>
            <xsl:attribute name="transUnit">
                <xsl:value-of select="@transUnit"/>
            </xsl:attribute>

            <xsl:if test="./init[@class='edu.diploma.metamodel.expressions.ExpressionList']">
                <ForInit class="ExpressionList">
                    <xsl:if test="./init/type[@class='edu.diploma.metamodel.types.PrimitiveType']">
                        <type class="PrimitiveType">
                            <name><xsl:value-of select="./init/type/name"/></name>
                        </type>
                    </xsl:if>
                    <expressions>
                        <xsl:for-each select="./init/expressions/expression">
                            <xsl:call-template name="Expression"/>
                        </xsl:for-each>
                    </expressions>

                </ForInit>
            </xsl:if>
            <xsl:if test="./init[@class='edu.diploma.metamodel.statements.StatementList']">
                <ForInit class="StatementList">
                    <xsl:if test="./init/type[@class='edu.diploma.metamodel.types.PrimitiveType']">
                        <type class="PrimitiveType">
                            <name><xsl:value-of select="./init/type/name"/></name>
                        </type>
                    </xsl:if>
                    <statements>
                        <xsl:for-each select="./init/statements/statement">
                            <xsl:call-template name="Statement"/>
                        </xsl:for-each>
                    </statements>
                </ForInit>
            </xsl:if>
            <xsl:if test="./condition[@class]">
                <ForCondition>
                    <xsl:call-template name="Condition"/>
                </ForCondition>
            </xsl:if>
            <xsl:if test="./action[@class]">
                <ForAction>
                    <xsl:if test="./action/type[@class='edu.diploma.metamodel.types.PrimitiveType']">
                        <type class="PrimitiveType">
                            <name><xsl:value-of select="./action/type/name"/></name>
                        </type>
                    </xsl:if>
                    <expressions>
                        <xsl:for-each select="./action/expressions/expression">
                            <xsl:call-template name="Expression"/>
                        </xsl:for-each>
                    </expressions>
                </ForAction>
            </xsl:if>
            <xsl:if test="./body[@class]">
                <ForBody>
                    <statements>
                        <xsl:for-each select="./body/statements/statement">
                            <xsl:call-template name="Statement"/>
                        </xsl:for-each>
                    </statements>
                </ForBody>
            </xsl:if>

        </ForStatement>
    </xsl:template>

    <xsl:template name="ContinueStatement">
        <ContinueStatement>
            <label><xsl:value-of select="./label"/></label>
        </ContinueStatement>
    </xsl:template>

    <xsl:template name="BreakStatement">
        <BreakStatement>
            <label><xsl:value-of select="./label"/></label>
        </BreakStatement>
    </xsl:template>

    <xsl:template name="Label">
        <label>
              <xsl:choose>
                  <xsl:when test="./expr[@class='edu.diploma.metamodel.literals.IntegerLiteral']">
                      <expr class="IntegerLiteral">
                          <type class="PrimitiveType">
                              <name><xsl:value-of select="./expr/type/name"/></name>
                          </type>
                          <value><xsl:value-of select="./expr/value"/></value>
                      </expr>
                  </xsl:when>
                  <xsl:when test="./expr[@class='edu.diploma.metamodel.literals.StringLiteral']">
                      <expr class="StringLiteral">
                          <type class="ClassType">
                              <name><xsl:value-of select="./expr/type/name"/></name>
                          </type>
                          <value><xsl:value-of select="./expr/value"/></value>
                      </expr>
                  </xsl:when>
                  <xsl:otherwise>
                      <expr class="SpecialLiteral">
                          <type>
                              <name>default</name>
                          </type>
                          <value>default</value>
                      </expr>
                  </xsl:otherwise>
              </xsl:choose>
              <states>
                  <statements>
                      <xsl:for-each select="./states/statements/statement">
                          <xsl:call-template name="Statement"/>
                      </xsl:for-each>
                  </statements>
              </states>
        </label>
    </xsl:template>

    <xsl:template name="SwitchStatement">
        <SwitchStatement>
            <xsl:attribute name="lineOfCode">
                <xsl:value-of select="@lineOfCode"/>
            </xsl:attribute>
            <xsl:attribute name="transUnit">
                <xsl:value-of select="@transUnit"/>
            </xsl:attribute>
            <SwitchCondition>
                <xsl:call-template name="Condition"/>
            </SwitchCondition>
            <SwitchCases>
                <xsl:for-each select="./cases/label">
                    <xsl:call-template name="Label"/>
                </xsl:for-each>
            </SwitchCases>
        </SwitchStatement>
    </xsl:template>

    <xsl:template name="AssignmentExpression">
        <AssignmentExpression>
            <xsl:attribute name="lineOfCode">
                <xsl:value-of select="@lineOfCode"/>
            </xsl:attribute>
            <xsl:attribute name="transUnit">
                <xsl:value-of select="@transUnit"/>
            </xsl:attribute>
            <xsl:if test="./type[@class='edu.diploma.metamodel.types.PrimitiveType']">
                <type class="PrimitiveType">
                    <name><xsl:value-of select="./type/name"/></name>
                </type>
            </xsl:if>
            <xsl:if test="./lhs[@class='edu.diploma.metamodel.expressions.VariableReference']">
                <lhs class="VariableReference">
                    <name><xsl:value-of select="./lhs/name"/></name>
                    <value><xsl:value-of select="./lhs/value"/></value>
                    <xsl:if test="./lhs/type[@class='edu.diploma.metamodel.types.PrimitiveType']">
                        <type class="PrimitiveType">
                            <xsl:value-of select="./lhs/type/name" />
                        </type>
                    </xsl:if>
                </lhs>
            </xsl:if>
            <xsl:if test="./lhs[@class='edu.diploma.metamodel.expressions.AttributeAccess']">
                <lhs class="AttributeAccess">
                    <type class="PrimitiveType">
                        <name><xsl:value-of select="./lhs/type/name"/></name>
                    </type>
                    <expr class="VariableReference">
                        <type class="PrimitiveType">
                            <name><xsl:value-of select="./lhs/expr/type/name"/></name>
                        </type>
                        <name><xsl:value-of select="./lhs/expr/name"/></name>
                    </expr>
                    <name><xsl:value-of select="./lhs/name"/></name>
                </lhs>
            </xsl:if>

            <!-- TODO: other lhs types support -->
            <xsl:if test="./rhs[@class='edu.diploma.metamodel.literals.IntegerLiteral']">
                <rhs class="IntegerLiteral">
                    <type class="PrimitiveType">
                        <name><xsl:value-of select="./rhs/type/name"/></name>
                    </type>
                    <value><xsl:value-of select="./rhs/value"/></value>
                </rhs>
            </xsl:if>
            <xsl:if test="./rhs[@class='edu.diploma.metamodel.literals.StringLiteral']">
                <rhs class="IntegerLiteral">
                    <type class="ClassType">
                        <name>java.lang.String</name>
                        <templates class="java.util.Collections$EmptyList"/>
                    </type>
                    <value><xsl:value-of select="./rhs/value"/></value>
                </rhs>
            </xsl:if>
            <xsl:if test="./rhs[@class='edu.diploma.metamodel.expressions.FunctionCall']">
                <rhs class="FunctionCall">
                    <xsl:call-template name="FunctionCall"/>
                </rhs>
            </xsl:if>
            <xsl:if test="./rhs[@class='edu.diploma.metamodel.expressions.BinaryExpression']">
                <xsl:call-template name="BinaryExpression"/>
            </xsl:if>
            <xsl:if test="./rhs[@class='edu.diploma.metamodel.expressions.VariableReference']">
                <rhs class="VariableReference">
                    <type class="edu.diploma.metamodel.types.PrimitiveType">
                        <name><xsl:value-of select="./rhs/type/name"/></name>
                    </type>
                    <name><xsl:value-of select="./rhs/name"/></name>
                </rhs>
            </xsl:if>

        </AssignmentExpression>
    </xsl:template>


    <xsl:template match="*/UnaryExpression" name="UnaryExpression">
        <UnaryExpression>
        <xsl:if test="./type[@class='edu.diploma.metamodel.types.PrimitiveType']">
            <type class="PrimitiveType">
                <name><xsl:value-of select="./type/name"/></name>
            </type>
        </xsl:if>
        <!-- TODO: other types support -->
        <xsl:if test="./operand[@class='edu.diploma.metamodel.expressions.VariableReference']">
            <operand class="VariableReference">
                <xsl:if test="./operand/type[@class='edu.diploma.metamodel.types.PrimitiveType']">
                    <type class="PrimitiveType">
                        <name><xsl:value-of select="./operand/type/name"/></name>
                    </type>
                </xsl:if>
                <name><xsl:value-of select="./operand/name"/></name>
            </operand>
        </xsl:if>
        <operation><xsl:value-of select="./operation"/></operation>
        <posfix><xsl:value-of select="./postfix"/></posfix>
        </UnaryExpression>
    </xsl:template>

    <xsl:template match="*/IfStatement" name="IfStatement">
        <IfStatement>
            <xsl:attribute name="lineOfCode">
                <xsl:value-of select="@lineOfCode"/>
            </xsl:attribute>
            <xsl:attribute name="transUnit">
                <xsl:value-of select="@transUnit"/>
            </xsl:attribute>
            <xsl:call-template name="Condition"/>
            <xsl:if test="./ifer[@class='edu.diploma.metamodel.statements.StatementBlock']">
                <IfBranch class="StatementBlock">
                    <statements>
                        <xsl:for-each select="./ifer/statements/statement">
                            <xsl:call-template name="Statement"/>
                        </xsl:for-each>
                    </statements>
                </IfBranch>
            </xsl:if>
            <xsl:if test="./elser[@class='edu.diploma.metamodel.statements.StatementBlock']">
                <ElseBranch class="StatementBlock">
                    <statements>
                        <xsl:for-each select="./elser/statements/statement">
                            <xsl:call-template name="Statement"/>
                        </xsl:for-each>
                    </statements>
                </ElseBranch>
            </xsl:if>
        </IfStatement>
    </xsl:template>

    <xsl:template match="*/params | */condition/lhs | */condition/rhs" name="BinaryExpression" mode="expression">

        <BinaryExpression>
            <!-- BinaryExpression as condition -->
            <xsl:if test="./condition[@class='edu.diploma.metamodel.expressions.BinaryExpression']">
                <xsl:if test="./condition/lhs[@class='edu.diploma.metamodel.expressions.VariableReference' or
                                                                  @class='edu.diploma.metamodel.literals.IntegerLiteral']">
                    <lhs>
                        <name><xsl:value-of select="./condition/lhs/name"/></name>
                        <value><xsl:value-of select="./condition/lhs/value"/></value>
                        <xsl:if test="./condition/lhs/type[@class='edu.diploma.metamodel.types.PrimitiveType']">
                            <type class="PrimitiveType">
                                <xsl:value-of select="./condition/lhs/type/name" />
                            </type>
                        </xsl:if>
                    </lhs>
                </xsl:if>
                <xsl:if test="./condition/lhs[@class='edu.diploma.metamodel.expressions.FunctionCall']">
                    <lhs>
                        <xsl:call-template name="FunctionCall"/>
                    </lhs>

                </xsl:if>
                <xsl:if test="./condition/lhs[@class='edu.diploma.metamodel.expressions.BinaryExpression']">
                    <lhs>
                        <xsl:apply-templates select="./condition/lhs" mode="expression"/>
                    </lhs>
                </xsl:if>

                <xsl:if test="./condition/rhs[@class='edu.diploma.metamodel.expressions.VariableReference' or
                                                                  @class='edu.diploma.metamodel.literals.IntegerLiteral']">
                    <rhs>
                        <name><xsl:value-of select="./condition/rhs/name"/></name>
                        <value><xsl:value-of select="./condition/rhs/value"/></value>
                        <xsl:if test="./condition/rhs/type[@class='edu.diploma.metamodel.types.PrimitiveType']">
                            <type class="PrimitiveType">
                                <xsl:value-of select="./condition/rhs/type/name" />
                            </type>
                        </xsl:if>
                    </rhs>
                </xsl:if>
                <xsl:if test="./condition/rhs[@class='edu.diploma.metamodel.expressions.BinaryExpression']">
                    <rhs>
                           <xsl:apply-templates select="./condition/rhs" mode="expression"/>
                    </rhs>
                </xsl:if>
                <xsl:if test="./condition/rhs[@class='edu.diploma.metamodel.expressions.FunctionCall']">
                    <rhs>
                           <xsl:for-each select="./condition/rhs">
                               <xsl:call-template name="FunctionCall"/>
                           </xsl:for-each>
                    </rhs>
                </xsl:if>
                <operation><xsl:value-of select="./condition/operation"/></operation>
            </xsl:if>
            <!-- BinaryExpression as it self (maybe rhs or lhs) -->
            <xsl:if test="./@class='edu.diploma.metamodel.expressions.BinaryExpression'">
                <xsl:call-template name="BinaryExpressionSelfBody"/>
            </xsl:if>
            <xsl:if test="./lhs/@class='edu.diploma.metamodel.expressions.BinaryExpression'">
                <xsl:apply-templates select="./lhs" mode="binaryExpressionSelfBody"/>
            </xsl:if>
            <xsl:if test="./rhs/@class='edu.diploma.metamodel.expressions.BinaryExpression'">
                <xsl:apply-templates select="./rhs" mode="binaryExpressionSelfBody"/>
            </xsl:if>
            <!-- BinaryExpression as expression -->
            <xsl:if test="./expression[@class='edu.diploma.metamodel.expressions.BinaryExpression']">
                <xsl:if test="./expression/type[@class='edu.diploma.metamodel.types.PrimitiveType']">
                    <type class="PrimitiveType">
                        <xsl:value-of select="./expression/type/name"/>
                    </type>
                </xsl:if>
                <!-- lhs -->
                <xsl:if test="./expression/lhs[@class='edu.diploma.metamodel.literals.StringLiteral']">
                    <type class="ClassType">
                        <name>java.lang.String</name>
                        <templates class="java.util.Collections$EmptyList"/>
                    </type>
                    <value><xsl:value-of select="./expression/lhs/value"/></value>
                </xsl:if>
                <xsl:if test="./expression/lhs[@class='edu.diploma.metamodel.expressions.VariableReference']">
                    <type class="PrimitiveType">
                        <name><xsl:value-of select="./expression/lhs/type/name"/></name>
                    </type>
                    <name><xsl:value-of select="./expression/lhs/name"/></name>
                </xsl:if>
                <!-- TODO: other lhs -->
                <!-- rhs -->
                <xsl:if test="./expression/rhs[@class='edu.diploma.metamodel.literals.StringLiteral']">
                    <type class="ClassType">
                        <name>java.lang.String</name>
                        <templates class="java.util.Collections$EmptyList"/>
                    </type>
                    <value><xsl:value-of select="./expression/rhs/value"/></value>
                </xsl:if>
                <xsl:if test="./expression/rhs[@class='edu.diploma.metamodel.expressions.VariableReference']">
                    <type class="PrimitiveType">
                        <name><xsl:value-of select="./expression/rhs/type/name"/></name>
                    </type>
                    <name><xsl:value-of select="./expression/rhs/name"/></name>
                </xsl:if>
                <!-- operator -->
                <operation><xsl:value-of select="./expression/operation"/></operation>
            </xsl:if>



        </BinaryExpression>
    </xsl:template>

    <xsl:template match="*/rhs[@class='edu.diploma.metamodel.expressions.BinaryExpression']" name="BinaryExpressionSelfBody" mode="binaryExpressionSelfBody">

        <!-- not need BinaryExpression open and close tag, need only body -->
        <xsl:if test="./type[@class='edu.diploma.metamodel.types.PrimitiveType']">
            <type class="PrimitiveType">
                <name><xsl:value-of select="./type/name"/></name>
            </type>
        </xsl:if>
        <!-- lhs -->
        <lhs>
            <xsl:if test="./lhs[@class='edu.diploma.metamodel.expressions.VariableReference']">
                    <VariableReference>
                        <xsl:if test="./lhs/type[@class='edu.diploma.metamodel.types.PrimitiveType']">
                            <type class="PrimitiveType">
                                <name><xsl:value-of select="./lhs/type/name"/></name>
                            </type>
                        </xsl:if>
                        <name><xsl:value-of select="./lhs/name"/></name>
                    </VariableReference>
            </xsl:if>
            <xsl:if test="./lhs[@class='edu.diploma.metamodel.literals.SpecialLiteral']">
                <SpecialLiteral>
                    <type class="PrimitiveType">
                        <name><xsl:value-of select="./lhs/type/name"/></name>
                    </type>
                    <value><xsl:value-of select="./lhs/value"/></value>
                </SpecialLiteral>
            </xsl:if>
            <xsl:if test="./lhs[@class='edu.diploma.metamodel.literals.IntegerLiteral']">
                <type class="PrimitiveType">
                    <name><xsl:value-of select="./lhs/type/name"/></name>
                </type>
                <value><xsl:value-of select="./lhs/value"/></value>
            </xsl:if>
        </lhs>
        <!-- TODO: improve type support -->
        <!-- rhs -->
        <rhs>

            <xsl:if test="./rhs[@class='edu.diploma.metamodel.literals.SpecialLiteral']">
                <SpecialLiteral>
                    <type class="PrimitiveType">
                        <name><xsl:value-of select="./rhs/type/name"/></name>
                    </type>
                    <value><xsl:value-of select="./rhs/value"/></value>
                </SpecialLiteral>
            </xsl:if>
            <xsl:if test="./rhs[@class='edu.diploma.metamodel.literals.IntegerLiteral']">
                <IntegerLiteral>
                    <type class="PrimitiveType">
                        <name><xsl:value-of select="./rhs/type/name"/></name>
                    </type>
                    <value><xsl:value-of select="./rhs/value"/></value>
                </IntegerLiteral>
            </xsl:if>
            <xsl:if test="./rhs[@class='edu.diploma.metamodel.expressions.VariableReference']">
                <VariableReference>
                    <xsl:if test="./rhs/type[@class='edu.diploma.metamodel.types.PrimitiveType']">
                        <type class="PrimitiveType">
                            <name><xsl:value-of select="./rhs/type/name"/></name>
                        </type>
                        <name><xsl:value-of select="./rhs/name"/></name>
                    </xsl:if>
                </VariableReference>
            </xsl:if>
        </rhs>
        <!-- operation -->
        <operation><xsl:value-of select="./operation"/></operation>

    </xsl:template>

    <xsl:template match="*/FunctionCall" name="FunctionCall">
        <FunctionCall>
            <xsl:attribute name="lineOfCode">
                <xsl:value-of select="@lineOfCode"/>
            </xsl:attribute>
            <xsl:attribute name="transUnit">
                <xsl:value-of select="@transUnit"/>
            </xsl:attribute>
        <xsl:if test="./type[@class='edu.diploma.metamodel.types.PrimitiveType']">
            <type class="PrimitiveType">
                <name><xsl:value-of select="./type/name"/></name>
            </type>
        </xsl:if>
        <!-- TODO: other types support -->
        <xsl:if test="./name">
            <name><xsl:value-of select="./name"/></name>
        </xsl:if>
        <xsl:if test="./caller[@class='edu.diploma.metamodel.expressions.AttributeAccess']">
            <caller class="AttributeAccess">
                <xsl:call-template name="CallerAttributeAccess"/>
            </caller>
        </xsl:if>
        <xsl:if test="./caller[@class='edu.diploma.metamodel.expressions.ConstructorCall']">
            <caller class="ConstructorCall">
                <xsl:call-template name="CallerConstructorCall"/>
            </caller>
        </xsl:if>
        <xsl:if test="./caller[@class='edu.diploma.metamodel.expressions.FunctionCall']">
            <caller class="FunctionCall">
                <xsl:call-template name="CallerFunctionCall"/>
            </caller>
        </xsl:if>
        <xsl:if test="./caller[@class='edu.diploma.metamodel.expressions.VariableReference']">
            <caller class="VariableReference">
                <xsl:call-template name="CallerVariableReference"/>
            </caller>
        </xsl:if>

        <!-- CALL as lhs -->
        <xsl:if test="./lhs/caller[@class='edu.diploma.metamodel.expressions.VariableReference']">
            <name><xsl:value-of select="./lhs/name"/></name>
            <caller class="VariableReference">
                <xsl:if test="./lhs/caller/type[@class='edu.diploma.metamodel.types.PrimitiveType']">
                    <type class="PrimitiveType">
                        <name><xsl:value-of select="./lhs/caller/type/name"/></name>
                    </type>
                </xsl:if>
                <name><xsl:value-of select="./lhs/caller/name"/></name>
            </caller>
            <xsl:apply-templates select="./lhs"/>
        </xsl:if>
        <!-- CALL as condition lhs -->
        <xsl:if test="./condition/lhs/caller[@class='edu.diploma.metamodel.expressions.VariableReference']">
            <name><xsl:value-of select="./condition/lhs/name"/></name>
            <caller class="VariableReference">
                <xsl:if test="./condition/lhs/caller/type[@class='edu.diploma.metamodel.types.PrimitiveType']">
                    <type class="PrimitiveType">
                        <name><xsl:value-of select="./condition/lhs/caller/type/name"/></name>
                    </type>
                </xsl:if>
                <name><xsl:value-of select="./condition/lhs/caller/name"/></name>
            </caller>
            <xsl:apply-templates select="./condition/lhs"/>
        </xsl:if>
        <!-- CALL as rhs -->
        <xsl:if test="./rhs/caller[@class='edu.diploma.metamodel.expressions.VariableReference']">
                <name><xsl:value-of select="./rhs/name"/></name>
                <caller class="VariableReference">
                    <xsl:if test="./rhs/caller/type[@class='edu.diploma.metamodel.types.PrimitiveType']">
                    <type class="PrimitiveType">
                        <name><xsl:value-of select="./rhs/caller/type/name"/></name>
                    </type>
                    </xsl:if>
                    <name><xsl:value-of select="./rhs/caller/name"/></name>
                </caller>
                <xsl:apply-templates select="./rhs"/>
        </xsl:if>
        <!-- other caller types support -->
        <xsl:if test="./params[@class]">
            <xsl:call-template name="CallFunctionParams"/>
        </xsl:if>
        </FunctionCall>
    </xsl:template>

    <xsl:template name="CallerVariableReference">
        <xsl:if test="./caller/type[@class='edu.diploma.metamodel.types.PrimitiveType']">
        <type class="PrimitiveType">
            <name><xsl:value-of select="./caller/type/name"/></name>
        </type>
        </xsl:if>
        <xsl:if test="./caller/type[@class='edu.diploma.metamodel.types.ClassType']">
            <type class="ClassType">
                <name><xsl:value-of select="./caller/type/name"/></name>
            </type>
        </xsl:if>
        <name><xsl:value-of select="./caller/name"/></name>

    </xsl:template>

    <xsl:template name="CallerFunctionCall">
        <xsl:for-each select="./caller[@class='edu.diploma.metamodel.expressions.FunctionCall']">
            <xsl:call-template name="FunctionCall"/>
        </xsl:for-each>
    </xsl:template>


    <xsl:template name="CallerConstructorCall">
        <type class="ClassType">
            <name><xsl:value-of select="./caller/type/name"/></name>
            <templates class="java.util.Collections$EmptyList"/>
        </type>
        <params class="java.util.Collections$EmptyList"/>
        <templates class="java.util.Collections$EmptyList"/>
        <heap><xsl:value-of select="./caller/heap"/></heap>
    </xsl:template>

    <xsl:template match="*/params/expression" name="AttributeAccess">
        <AttributeAccess>
            <xsl:if test="./type[@class='edu.diploma.metamodel.types.PrimitiveType']">
                <type class="PrimitiveType">
                    <name><xsl:value-of select="./type/name"/></name>
                </type>
            </xsl:if>
            <!-- TODO: other types support -->
            <xsl:if test="./expr[@class='edu.diploma.metamodel.expressions.VariableReference']">
                <VariableReference>
                    <xsl:if test="./type[@class='edu.diploma.metamodel.types.PrimitiveType']">
                        <type class="PrimitiveType">
                            <name><xsl:value-of select="./type/name"/></name>
                        </type>
                    </xsl:if>
                    <name><xsl:value-of select="./expr/name"/></name>
                </VariableReference>
                <name><xsl:value-of select="./name"/></name>
            </xsl:if>
        </AttributeAccess>
    </xsl:template>

    <xsl:template name="CallerAttributeAccess">
        <AttributeAccess>
            <xsl:if test="./caller/type[@class='edu.diploma.metamodel.types.PrimitiveType']">
                <type class="PrimitiveType">
                    <name><xsl:value-of select="./type/name"/></name>
                </type>
            </xsl:if>
            <!-- TODO: other types support -->
            <xsl:if test="./caller/expr[@class='edu.diploma.metamodel.expressions.VariableReference']">
                <VariableReference>
                    <xsl:if test="./caller/expr/type[@class='edu.diploma.metamodel.types.PrimitiveType']">
                        <type class="PrimitiveType">
                            <name><xsl:value-of select="./caller/expr/type/name"/></name>
                        </type>
                    </xsl:if>
                    <name><xsl:value-of select="./caller/expr/name"/></name>
                </VariableReference>
                <name><xsl:value-of select="./caller/name"/></name>
            </xsl:if>
        </AttributeAccess>
    </xsl:template>

    <xsl:template match="*/variable/value | */lhs | */rhs" name="CallFunctionParams" priority="0">
        <xsl:element name="params">
            <xsl:choose>
                <xsl:when test="./params[@class='java.util.LinkedList']">
                    <xsl:attribute name="emptyList">false</xsl:attribute>
                    <xsl:if test="./params/expression[@class='edu.diploma.metamodel.literals.StringLiteral']">

                        <StringLiteral>
                            <type class="ClassType">
                                <name><xsl:value-of select="./params/expression/type/name"/></name>
                            </type>
                            <value><xsl:value-of select="./params/expression/value"/></value>
                        </StringLiteral>
                    </xsl:if>
                    <xsl:if test="./params/expression[@class='edu.diploma.metamodel.literals.IntegerLiteral']">
                        <IntegerLiteral>
                            <type class="PrimitiveType">
                                <name><xsl:value-of select="./params/expression/type/name"/></name>
                            </type>
                            <value><xsl:value-of select="./params/expression/value"/></value>
                        </IntegerLiteral>
                    </xsl:if>
                    <xsl:if test="./params/expression[@class='edu.diploma.metamodel.literals.SpecialLiteral']">
                        <SpecialLiteral>
                            <type class="PrimitiveType">
                                <name>unknown</name>
                            </type>
                            <value><xsl:value-of select="./params/expression/value"/></value>
                        </SpecialLiteral>
                    </xsl:if>
                    <xsl:if test="./params/expression[@class='edu.diploma.metamodel.expressions.AttributeAccess']">
                        <xsl:apply-templates select="./params/expression"/>
                    </xsl:if>
                    <xsl:if test="./params/expression[@class='edu.diploma.metamodel.expressions.BinaryExpression']">
                        <xsl:apply-templates select="./params" mode="expression"/>
                    </xsl:if>
                    <xsl:if test="./params/expression[@class='edu.diploma.metamodel.expressions.VariableReference']">
                        <VariableReference>
                            <xsl:if test="./params/expression/type[@class='edu.diploma.metamodel.types.PrimitiveType']">
                                <type class="PrimitiveType">
                                    <name><xsl:value-of select="./params/expression/type/name"/></name>
                                </type>
                            </xsl:if>
                            <!-- TODO : other types support -->
                            <name><xsl:value-of select="./params/expression/name"/></name>
                        </VariableReference>
                    </xsl:if>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:attribute name="emptyList">true</xsl:attribute>
                </xsl:otherwise>
            </xsl:choose>

            <!-- TODO: other types and expressions support -->
        </xsl:element>
    </xsl:template>

    <xsl:template name="WhileStatement">
        <WhileStatement>
            <xsl:attribute name="lineOfCode">
                <xsl:value-of select="@lineOfCode"/>
            </xsl:attribute>
            <xsl:attribute name="transUnit">
                <xsl:value-of select="@transUnit"/>
            </xsl:attribute>
            <xsl:call-template name="Condition"/>
            <xsl:if test="./body[@class='edu.diploma.metamodel.statements.StatementBlock']">
                <StatementBlock>
                    <statements>
                        <xsl:for-each select="./body/statements/statement">
                            <xsl:call-template name="Statement"/>
                        </xsl:for-each>
                    </statements>
                </StatementBlock>
            </xsl:if>

        </WhileStatement>
    </xsl:template>

    <xsl:template name="Condition">
        <xsl:if test="./condition[@class='edu.diploma.metamodel.expressions.BinaryExpression']">
            <condition class="BinaryExpression">
                <xsl:call-template name="BinaryExpression"/>
            </condition>
        </xsl:if>
        <xsl:if test="./condition[@class='edu.diploma.metamodel.literals.BooleanLiteral']">
            <condition class="BooleanLiteral">
                <type class="PrimitiveType">
                    <name><xsl:value-of select="./condition/type/name"/></name>
                </type>
                <value><xsl:value-of select="./condition/value"/></value>
            </condition>
        </xsl:if>
        <xsl:if test="./condition[@class='edu.diploma.metamodel.expressions.VariableReference']">
            <condition class="VariableReference">
                <type class="PrimitiveType">
                    <name><xsl:value-of select="./condition/type/name"/></name>
                </type>
                <name><xsl:value-of select="./condition/name"/></name>
            </condition>
        </xsl:if>
        <xsl:if test="./condition[@class='edu.diploma.metamodel.expressions.FunctionCall']">
            <condition class="FunctionCall">
                <xsl:for-each select="./condition[@class='edu.diploma.metamodel.expressions.FunctionCall']">
                    <xsl:call-template name="FunctionCall"/>
                </xsl:for-each>
            </condition>
        </xsl:if>
    </xsl:template>

    <xsl:template name="VariableDeclStatement">
        <VariableDeclStatement>
            <xsl:attribute name="lineOfCode">
                <xsl:value-of select="@lineOfCode"/>
            </xsl:attribute>
            <xsl:attribute name="transUnit">
                <xsl:value-of select="@transUnit"/>
            </xsl:attribute>
            <variable>
                <xsl:attribute name="lineOfCode">
                    <xsl:value-of select="@lineOfCode"/>
                </xsl:attribute>
                <xsl:attribute name="transUnit">
                    <xsl:value-of select="@transUnit"/>
                </xsl:attribute>
                <name><xsl:value-of select="./variable/name"/></name>
                <visibility><xsl:value-of select="./variable/visibility"/></visibility>
                <modifiers class="java.util.LinkedList"/>
                <annotations class="java.util.LinkedList"/>
                <xsl:if test="./variable/type[@class='edu.diploma.metamodel.types.PrimitiveType']">
                    <type class="PrimitiveType">
                        <name><xsl:value-of select="./variable/type/name"/></name>
                        <xsl:if test="./variable/value[@class='edu.diploma.metamodel.literals.IntegerLiteral']">
                            <value class="IntegerLiteral">
                                <xsl:value-of select="./variable/value/value"/>
                            </value>
                        </xsl:if>
                        <xsl:if test="./variable/value[@class='edu.diploma.metamodel.expressions.VariableReference']">
                            <value class="VariableReference">
                                <type class="PrimitiveType">
                                    <name><xsl:value-of select="./variable/value/type/name"/></name>
                                </type>
                                <name><xsl:value-of select="./variable/value/name"/></name>
                            </value>
                        </xsl:if>
                        <!-- TODO: other branches -->
                    </type>
                </xsl:if>
                <xsl:if test="./variable/type[@class='edu.diploma.metamodel.types.ClassType']">
                    <type class="ClassType">
                        <name><xsl:value-of select="./variable/type/name"/></name>
                        <templates class="java.util.Collections$EmptyList"/>
                    </type>
                    <xsl:if test="./variable/value[@class='edu.diploma.metamodel.expressions.ConstructorCall']">
                        <value class="ConstructorCall">
                            <type class="ClassType">
                                <name><xsl:value-of select="./variable/value/type/name"/></name>
                                <templates class="java.util.Collections$EmptyList"/>
                            </type>
                            <xsl:apply-templates select="./variable/value"/>
                        </value>
                        <templates class="java.util.Collections$EmptyList"/>
                        <heap><xsl:value-of select="./variable/value/heap"/></heap>
                    </xsl:if>
                    <xsl:if test="./variable/value[@class='edu.diploma.metamodel.literals.StringLiteral']">
                        <value class="StringLiteral">
                            <type class="ClassType">
                                <name><xsl:value-of select="./variable/value/type/name"/></name>
                                <templates class="java.util.Collections$EmptyList"/>
                            </type>
                            <value><xsl:value-of select="./variable/value/value"/></value>
                        </value>
                    </xsl:if>
                    <xsl:if test="./variable/value[@class='edu.diploma.metamodel.literals.SpecialLiteral']">
                        <value class="SpecialLiteral">
                            <type class="PrimitiveType">
                                <name><xsl:value-of select="./variable/value/type/name"/></name>
                                <templates class="java.util.Collections$EmptyList"/>
                            </type>
                            <value><xsl:value-of select="./variable/value/value"/></value>
                        </value>
                    </xsl:if>
                </xsl:if>
                <!-- TODO: other branches -->

            </variable>
        </VariableDeclStatement>
    </xsl:template>






</xsl:stylesheet>