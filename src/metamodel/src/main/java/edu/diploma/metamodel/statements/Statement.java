package edu.diploma.metamodel.statements;

import edu.diploma.metamodel.Entity;
import edu.diploma.metamodel.Location;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Default;

/**
 * Created by alexander on 4/26/14.
 */
@Default
public abstract class Statement implements Entity, Location {

    @Attribute(name="lineOfCode")
    protected int lineOfCode = 0;

    @Attribute(name="transUnit")
    protected String transUnit = "UNDEFINED";

    @Override
    public void setLineOfCode(int loc) {
        lineOfCode = loc;
    }

    @Override
    public int getLineOfCode() {
        return lineOfCode;
    }

    @Override
    public String getTransUnit() {
        return transUnit;
    }

    @Override
    public void setTransUnit(String transUnit) {
        this.transUnit = transUnit;
    }
}
