package edu.diploma.metamodel;

/**
 * Created by rudolph on 19.04.15.
 */
public interface Location {

    public void setLineOfCode(int loc);
    public int getLineOfCode();

    public void setTransUnit(String transUnit);
    public String getTransUnit();
}
