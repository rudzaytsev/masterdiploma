xquery version "3.0";

declare variable $mydoc external;
for $funcCall in $mydoc/start//FunctionCall

where ( fn:compare($funcCall/name,'exit') = 0 and fn:compare($funcCall/caller/name,'System') = 0 )
      or
      ( fn:compare($funcCall/name,'exit') = 0
        and
        fn:compare($funcCall/caller/FunctionCall/name,'getRuntime') = 0
        and
        fn:compare($funcCall/caller/FunctionCall/caller/name,'Runtime') = 0
      )

return ($funcCall)
