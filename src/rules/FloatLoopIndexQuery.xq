xquery version "3.0";
declare variable $mydoc external;
for $forStatement in $mydoc/start//ForStatement
    where fn:compare($forStatement/ForInit/statements/VariableDeclStatement/variable/type/name,'float') = 0
        return ($forStatement)