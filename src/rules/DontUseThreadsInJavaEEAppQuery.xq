xquery version "3.0";

declare variable $mydoc external;
for $type in $mydoc/start//type[./@class='ClassType']

where fn:compare($type/name,'Thread') = 0 or fn:compare($type/name,'Runnable') = 0

return ($type/../..)