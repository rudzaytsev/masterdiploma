xquery version "3.0";
declare variable $mydoc external;
for $ifStmt in $mydoc/start//statements//IfStatement[./condition//FunctionCall]
where some $FuncCall in $ifStmt/condition//FunctionCall satisfies
(
    fn:compare($FuncCall/name,'equals') = 0
    and
    fn:compare( $FuncCall/params/SpecialLiteral/value,'null') = 0
)

return ($ifStmt)