xquery version "3.0";
declare variable $mydoc external;
let $switchs := $mydoc/start//SwitchStatement
for $switch in $switchs
for $labels in $switch/SwitchCases//label[last()]
    where every $label in $labels satisfies
        $label/expr/type/name != 'default' and
        $label/expr/@class != 'SpecialLiteral'
        return ($switch)
