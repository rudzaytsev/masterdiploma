xquery version "3.0";
declare variable $mydoc external;
for $forStatement in $mydoc/start//ForStatement
where fn:compare($forStatement/ForInit/expressions/AssignmentExpression/lhs/name,
                 $forStatement/ForAction/expressions/UnaryExpression/operand/name) != 0

      or
      fn:compare($forStatement/ForInit/expressions/AssignmentExpression/lhs/name,
                 $forStatement/ForAction/expressions/AssignmentExpression/lhs/name) != 0
    return ($forStatement)