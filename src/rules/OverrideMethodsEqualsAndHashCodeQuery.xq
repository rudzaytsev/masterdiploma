xquery version "3.0";

declare variable $mydoc external;
let $res :=
<result>
{

       for $classDecl in $mydoc/start//ClassDecl
       return  <part>{
            <data>
            <rawClassDecl>
               {$classDecl}
            </rawClassDecl>
            <equalsCounter>
                {fn:count($classDecl//FunctionDecl[./name='equals'])}
            </equalsCounter>
            <hashCodeCounter>
                {fn:count($classDecl//FunctionDecl[./name='hashCode'])}
            </hashCodeCounter>
            </data>
       }</part>
}
</result>

for $part in $res//part
    where ( $part/data/equalsCounter=1 and $part/data/hashCodeCounter=0 )
          or
          ( $part/data/equalsCounter=0 and $part/data/hashCodeCounter=1 )

return ($part/data/rawClassDecl)
