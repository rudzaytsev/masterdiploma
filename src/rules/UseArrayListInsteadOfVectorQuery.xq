xquery version "3.0";

declare variable $mydoc external;
for $type in $mydoc/start//type[./@class='ClassType']

where fn:compare($type/name,'Vector') = 0 or fn:compare($type/name,'java.util.Vector') = 0

return ($type/../..)