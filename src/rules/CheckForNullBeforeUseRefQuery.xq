xquery version "3.0";
declare variable $mydoc external;
for $ifStmt in $mydoc/start//statements//IfStatement[./condition/BinaryExpression]
    where
        (
            fn:compare($ifStmt/condition/BinaryExpression/lhs/FunctionCall/caller/name,
                       $ifStmt/condition/BinaryExpression/rhs/BinaryExpression/lhs/VariableReference/name)
                         = 0
        and
            fn:compare( $ifStmt/condition/BinaryExpression/rhs/BinaryExpression/rhs/SpecialLiteral/value,
                        'null') = 0
        )
        or
        (
            fn:compare($ifStmt/condition/BinaryExpression/lhs/FunctionCall/caller/name,
                       $ifStmt/condition/BinaryExpression/rhs/BinaryExpression/rhs/VariableReference/name)
                       = 0
        and
            fn:compare($ifStmt/condition/BinaryExpression/rhs/BinaryExpression/lhs/SpecialLiteral/value,
                       'null') = 0
        )
        return ($ifStmt)