xquery version "3.0";

declare variable $mydoc external;
let $res :=
<result>
    {

    for $classDecl in $mydoc/start//ClassDecl
        where some $funcDecl in $classDecl//FunctionDecl satisfies
              fn:compare($funcDecl/name,$classDecl/name) = 0
              and fn:compare($funcDecl/visibility,'PRIVATE') = 0
    return  <part>{
    <data>
        <rawClassDecl>
            {$classDecl}
        </rawClassDecl>
        <constructorCounter>
            {fn:count($classDecl//FunctionDecl[./name=$classDecl/name])}
        </constructorCounter>
        <privateConstructorCounter>
            {fn:count($classDecl//FunctionDecl[./name=$classDecl/name and ./visibility='PRIVATE'])}
        </privateConstructorCounter>
        <staticMethodsCounter>
            {fn:count($classDecl//FunctionDecl//modifiers[./string='static'])}
        </staticMethodsCounter>
    </data>
    }</part>
    }
</result>

for $part in $res//part
    where $part/data/constructorCounter=$part/data/privateConstructorCounter
          and
          $part/data/staticMethodsCounter=0
    return ($part/data/rawClassDecl)
