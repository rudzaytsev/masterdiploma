xquery version "3.0";

declare variable $mydoc external;
for $varDecl in $mydoc/start//statements//VariableDeclStatement[./variable/type/name='Thread'],
    $funcCall in $mydoc/start//statements//FunctionCall

    where ( fn:compare($funcCall/name,'run') = 0 and fn:compare($funcCall/caller/type/name,'Thread') = 0 )
          or (fn:compare($funcCall/name,'run') = 0 and
              fn:compare($funcCall/caller/name,$varDecl/variable/name) = 0)
        return ($funcCall)


