xquery version "3.0";

declare variable $mydoc external;
for $classDecl in $mydoc/start//ClassDecl
where fn:compare($classDecl/inherits/type/name,'Error') = 0
      or fn:compare($classDecl/inherits/type/name,'java.lang.Error') = 0

return ($classDecl)