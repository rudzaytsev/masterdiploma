xquery version "3.0";

declare variable $mydoc external;
for $assignment in $mydoc/start//AssignmentExpression

where fn:compare($assignment/lhs[./@class="VariableReference"]/name,
                 $assignment/rhs[./@class="VariableReference"]/name) = 0
return ($assignment)