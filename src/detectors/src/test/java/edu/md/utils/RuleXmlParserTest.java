package edu.md.utils;

import edu.md.rules.Rule;
import junit.framework.Assert;
import org.junit.Test;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;

public class RuleXmlParserTest {

    @Test
    public void testXmlParser() throws ParserConfigurationException, SAXException, IOException {

        File filePath = new File("../rules/FloatLoopIndex.xml");
        SAXParserFactory factory = SAXParserFactory.newInstance();
        SAXParser parser = factory.newSAXParser();
        RuleXmlParser ruleXmlParser = new RuleXmlParser();
        parser.parse(filePath, ruleXmlParser);
        Rule rule = ruleXmlParser.getRule();
        System.out.println(rule);
        Assert.assertEquals("FloatLoopIndex",rule.getName());
        Assert.assertTrue(rule.getDescription() != "");
        Assert.assertEquals("FloatLoopIndexQuery.xq",rule.getQueryFileName());

    }

}