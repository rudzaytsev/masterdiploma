package edu.md.utils;

import org.junit.Test;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.ByteArrayInputStream;
import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class ViolationDataXmlParserTest {


    @Test
    public void testParser() throws ParserConfigurationException, SAXException, IOException {

        String source = "<start><IfStatement lineOfCode=\"123\" transUnit=\"Open.java\" ></IfStatement></start>";

        SAXParserFactory factory = SAXParserFactory.newInstance();
        SAXParser parser = null;
        parser = factory.newSAXParser();
        ViolationDataXmlParser dataParser = new ViolationDataXmlParser();
        parser.parse(new ByteArrayInputStream(source.getBytes()),dataParser);
        long line = dataParser.getLineOfCode();
        String transUnit = dataParser.getTranslationUnit();
        assertEquals(123L,line);
        assertEquals("Open.java",transUnit);


    }

}