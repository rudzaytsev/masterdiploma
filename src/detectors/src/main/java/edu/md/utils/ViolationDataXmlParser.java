package edu.md.utils;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * Created by rudolph on 11.05.15.
 */
public class ViolationDataXmlParser extends DefaultHandler {

    private String translationUnit;
    private long lineOfCode;

    boolean firstLineOfCodeAttrib = true;
    boolean firstTranslationUnitAttrib = true;

    public String getTranslationUnit() {
        return translationUnit;
    }

    public void setTranslationUnit(String translationUnit) {
        this.translationUnit = translationUnit;
    }

    public long getLineOfCode() {
        return lineOfCode;
    }

    public void setLineOfCode(int lineOfCode) {
        this.lineOfCode = lineOfCode;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        //super.startElement(uri, localName, qName, attributes);

        if(firstLineOfCodeAttrib) {
            String value =  attributes.getValue("lineOfCode");
            if(value != null){
                lineOfCode = Long.parseLong(value);
                firstLineOfCodeAttrib = false;
            }
        }
        if(firstTranslationUnitAttrib){
            String val = attributes.getValue("transUnit");
            if(val != null){
                translationUnit = val;
                firstTranslationUnitAttrib = false;
            }
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        super.endElement(uri, localName, qName);
    }


}
