package edu.md.utils;

import edu.md.rules.Rule;
import edu.md.rules.XQueryRule;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * Created by rudolph on 05.04.15.
 */
public class RuleXmlParser extends DefaultHandler {

    private Rule rule = new XQueryRule();

    private String currentElement = null;

    private StringBuilder descriptBuilder = new StringBuilder();

    @Override
    public void startDocument() throws SAXException {
        System.out.println("Start XML parsing");
    }

    @Override
    public void endDocument() throws SAXException {
        rule.setDescription(descriptBuilder.toString());
        System.out.println("End XML parsing");
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
       this.currentElement = qName;
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        this.currentElement = "";
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {

        if(currentElement.equals("name")){
            rule.setName(new String(ch,start,length).trim());
        }
        else if(currentElement.equals("description")){
            descriptBuilder.append(new String(ch,start,length).trim());

        }
        else if(currentElement.equals("queryfile")){
            rule.setQueryFileName(new String(ch,start,length).trim());
        }
    }

    public Rule getRule() {
        return rule;
    }

    public void setRule(Rule rule) {
        this.rule = rule;
    }
}
