package edu.md.utils;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * Created by rudolph on 03.04.15.
 */
public class Utils {


    public static Document getXmlDocument(String filePath) throws Exception {

        FileInputStream file = null;
        try {
            file = new FileInputStream(new File(filePath));
        }
        catch (IOException ex){
            throw new Exception("IOException : " + ex.getMessage());
        }
        DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = null;
        Document xmlDocument = null;
        try {
            builder = builderFactory.newDocumentBuilder();
            xmlDocument = builder.parse(file);
        } catch (ParserConfigurationException e) {
            throw new Exception("ParserConfigurationException :  " + e.getMessage());
        }
        catch (SAXException se){
            throw new Exception("SAXException : " + se.getMessage());
        }

        return xmlDocument;
    }
}
