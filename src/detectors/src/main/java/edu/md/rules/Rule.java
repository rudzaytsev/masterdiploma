package edu.md.rules;

import java.util.List;

/**
 * Common Interface for all rules, which
 * need to be detected
 *
 * Created by rudolph on 22.03.15.
 */
public interface Rule {

    String getName();
    void setName(String name);

    String getDescription();
    void setDescription(String description);

    List<RuleViolation> matching(String astFile);

    String getQueryFileName();

    void setQueryFileName(String queryFileName);
}
