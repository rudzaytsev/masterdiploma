package edu.md.rules;

import java.util.List;

/**
 * Created by rudolph on 03.04.15.
 */
public class RuleWrapper {

    private Rule rule;
    private boolean selected = false;

    public RuleWrapper(Rule rule) {
        this.rule = rule;
    }

    public Rule getRule() {
        return rule;
    }

    public void setRule(Rule rule) {
        this.rule = rule;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }


    public void setDescription(String description) {
        rule.setDescription(description);
    }

    public String getDescription() {
        return rule.getDescription();
    }

    public void setName(String name) {
        rule.setName(name);
    }

    public String getName() {
        return rule.getName();
    }

    public void setQueryFileName(String queryFileName) {
        rule.setQueryFileName(queryFileName);
    }

    public String getQueryFileName() {
        return rule.getQueryFileName();
    }

    public List<RuleViolation> matching(String astFile) {
        return rule.matching(astFile);
    }
}
