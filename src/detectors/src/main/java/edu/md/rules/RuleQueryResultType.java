package edu.md.rules;


/**
 * Represents possible result types for query to the metamodel
 *
 * Created by rudolph on 22.03.15.
 */
public enum RuleQueryResultType {

    SINGLE(0), MULTIPLE(1);

    private final int value;

    private RuleQueryResultType(int val){
        this.value = val;
    }


}
