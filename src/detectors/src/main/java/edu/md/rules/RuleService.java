package edu.md.rules;

import edu.md.utils.RuleXmlParser;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by rudolph on 03.04.15.
 */
public class RuleService {

    public static final String RULES_DIR = "./rules";

    public static List<RuleWrapper> getAvailableRules(){
        List<RuleWrapper> ruleWrappers = new ArrayList<>();
        File dir = new File(RULES_DIR);
        if(dir.exists() && dir.isDirectory()){

            File[] rulesFiles = dir.listFiles(new FilenameFilter() {
                @Override
                public boolean accept(File dir, String name) {

                    return name.matches("^[a-zA-Z][0-9a-zA-Z]*.xml$");
                }
            });
            for(File ruleFile : rulesFiles){
               RuleWrapper wrapper = RuleService.parseRule(ruleFile.getAbsolutePath());
               ruleWrappers.add(wrapper);
            }

        }
        return ruleWrappers;
    }

    private static RuleWrapper parseRule(String filePath){

        RuleWrapper wrapper = null;
        try {

            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser parser = factory.newSAXParser();
            RuleXmlParser ruleXmlParser = new RuleXmlParser();
            parser.parse(filePath, ruleXmlParser);
            Rule rule = ruleXmlParser.getRule();
            wrapper = new RuleWrapper(rule);

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return wrapper;
    }
}
