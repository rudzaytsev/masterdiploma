package edu.md.rules;

import edu.md.utils.ViolationDataXmlParser;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.ByteArrayInputStream;
import java.io.IOException;

/**
 * Created by rudolph on 22.03.15.
 */
public class RuleViolation {

    private Long lineOfCode;
    private String translationUnit;

    private String ruleName;
    private String message;
    private String rawAstViolationPiece;

    public Long getLineOfCode() {
        return lineOfCode;
    }

    public void setLineOfCode(Long lineOfCode) {
        this.lineOfCode = lineOfCode;
    }

    public String getRuleName() {
        return ruleName;
    }

    public void setRuleName(String ruleName) {
        this.ruleName = ruleName;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getRawAstViolationPiece() {
        return rawAstViolationPiece;
    }

    public void setRawAstViolationPiece(String rawAstViolationPiece) {
        this.rawAstViolationPiece = rawAstViolationPiece;
    }

    public String getTranslationUnit() {
        return translationUnit;
    }

    public void setTranslationUnit(String translationUnit) {
        this.translationUnit = translationUnit;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RuleViolation)) return false;

        RuleViolation violation = (RuleViolation) o;

        if (lineOfCode != null ? !lineOfCode.equals(violation.lineOfCode) : violation.lineOfCode != null) return false;
        if (!message.equals(violation.message)) return false;
        if (!rawAstViolationPiece.equals(violation.rawAstViolationPiece)) return false;
        if (!ruleName.equals(violation.ruleName)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = lineOfCode != null ? lineOfCode.hashCode() : 0;
        result = 31 * result + ruleName.hashCode();
        result = 31 * result + message.hashCode();
        result = 31 * result + rawAstViolationPiece.hashCode();
        return result;
    }

    public void extractViolationLocation(){
        this.extractDefectLocation(this.rawAstViolationPiece);
    }

    private void extractDefectLocation(String source){
        if(source == null){
            return;
        }
        SAXParserFactory factory = SAXParserFactory.newInstance();
        SAXParser parser = null;
        try {
            parser = factory.newSAXParser();
            ViolationDataXmlParser dataParser = new ViolationDataXmlParser();
            parser.parse(new ByteArrayInputStream(source.getBytes()),dataParser);
            this.lineOfCode = dataParser.getLineOfCode();
            this.translationUnit = dataParser.getTranslationUnit();

        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
