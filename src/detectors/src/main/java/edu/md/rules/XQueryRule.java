package edu.md.rules;

import edu.md.xquery.XQueryExecutor;
import net.sf.saxon.s9api.SaxonApiException;
import net.sf.saxon.s9api.XdmItem;
import net.sf.saxon.s9api.XdmValue;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by rudolph on 02.04.15.
 */
public class XQueryRule extends AbstractRule {


    @Override
    public List<RuleViolation> matching(String astFile) {
        List<RuleViolation> ruleViolations;
        try {
            XdmValue result = this.executeRuleQuery(astFile);
            ruleViolations = this.getRuleViolations(result);
        }
        catch (SaxonApiException | IOException e){
            e.printStackTrace();
            List<RuleViolation> emptyViolationList = new ArrayList<>();
            return emptyViolationList;
        }

        return ruleViolations;

    }

    private XdmValue executeRuleQuery(String dataFileName) throws IOException, SaxonApiException {
        XQueryExecutor executor = new XQueryExecutor();
        String fullQueryFileName = RuleService.RULES_DIR + File.separator + this.queryFileName;
        return executor.executeQuery(fullQueryFileName, dataFileName);
    }

    private List<RuleViolation> getRuleViolations(XdmValue xdmValue){

        List<RuleViolation> violations = new ArrayList<>();
        for(int i = 0; i < xdmValue.size(); i++){
            XdmItem item = xdmValue.itemAt(i);
            RuleViolation violation = new RuleViolation();
            violation.setRawAstViolationPiece(item.toString());
            violation.extractViolationLocation();
            violation.setRuleName(this.name);
            violation.setMessage(this.description);
            violations.add(violation);
        }

        return makeUnique(violations);
    }

    private List<RuleViolation> makeUnique(List<RuleViolation> violations){
        Set<RuleViolation> violationSet = new HashSet<>(violations);
        return new ArrayList<RuleViolation>(violationSet);
    }



}
