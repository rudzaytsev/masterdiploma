package edu.md.rules;

/**
 * Created by rudolph on 22.03.15.
 */
public abstract class AbstractRule implements Rule {

    String name;
    String description;

    String queryFileName;


    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }


    @Override
    public String getQueryFileName() {
        return queryFileName;
    }

    @Override
    public void setQueryFileName(String queryFileName) {
        this.queryFileName = queryFileName;
    }

    @Override
    public String toString() {
        return "{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", queryFileName='" + queryFileName + '\'' +
                '}';
    }
}
