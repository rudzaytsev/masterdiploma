package edu.md.detectors;

import edu.md.rules.Rule;
import edu.md.rules.RuleService;
import edu.md.rules.RuleViolation;
import edu.md.rules.RuleWrapper;
import edu.md.transform.XsltTransformer;
import edu.md.xquery.XQueryExecutor;
import net.sf.saxon.s9api.SaxonApiException;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents bug detector
 *
 * Created by rudolph on 22.03.15.
 */
public class Detector {

    List<Rule> rules = new ArrayList<>();
    List<RuleViolation> ruleViolations = new ArrayList<>();

    String metamodelFile = null;
    boolean metamodelSelected = false;

    public void findRuleViolations() throws Exception {
        if(!metamodelSelected){
            throw new Exception("MetaModelFile Not selected");
        }
        for( Rule currentRule : rules ){
            List<RuleViolation> currentRuleViolations =
                                    currentRule.matching(metamodelFile);
            ruleViolations.addAll(currentRuleViolations);
        }
    }

    public List<RuleViolation> getTotalBugs(){
        return ruleViolations;
    }

    public List<Rule> getRules() {
        return rules;
    }

    public void setRules(List<Rule> rules) {
        this.rules = rules;
    }

    public void addRule(Rule rule){
        if(!rules.contains(rule)){
            rules.add(rule);
        }
    }

    public String getMetamodelFile() {
        return metamodelFile;
    }

    public void setMetamodelFile(String metamodelFile) {
        this.metamodelFile = metamodelFile;
        metamodelSelected = true;
    }

    public static void main(String[] args){


        XsltTransformer transformer = new XsltTransformer();
        XQueryExecutor executor = new XQueryExecutor();
        List<RuleWrapper> ruleWrappers = RuleService.getAvailableRules();
        System.out.println("----------------------------------");
        for(RuleWrapper wrapper : ruleWrappers){
            System.out.println(wrapper.getQueryFileName());
        }
        System.out.println("----------------------------------");
        try {
            transformer.transform();
            Detector detector = new Detector();
            //for experiment
            //List<Rule> experRules = new ArrayList<>();
            for(RuleWrapper wrapper : ruleWrappers){
                detector.addRule(wrapper.getRule());
                // for experiment
                /*
                experRules.add(wrapper.getRule());
                experRules.add(wrapper.getRule());
                experRules.add(wrapper.getRule());
                experRules.add(wrapper.getRule());
                experRules.add(wrapper.getRule());
                experRules.add(wrapper.getRule());
                experRules.add(wrapper.getRule());
                experRules.add(wrapper.getRule());
                experRules.add(wrapper.getRule());
                experRules.add(wrapper.getRule());
                */
            }
            // for experiment
            //detector.setRules(experRules);


            detector.setMetamodelFile("./ast.xml");

            long startTime = System.currentTimeMillis();
            detector.findRuleViolations();
            long finishTime = System.currentTimeMillis();
            double diffSeconds = (finishTime - startTime) / 1000.0;
            System.out.println("Time in seconds = " + diffSeconds);

            List<RuleViolation> bugs = detector.getTotalBugs();
            System.out.println("Number of bugs = " + bugs.size());

            System.out.println("--------------[BUGS]-------------------");
            for(RuleViolation bug : bugs){
                System.out.println(bug.getRuleName());
                System.out.println(bug.getMessage());
                System.out.println(bug.getLineOfCode());
                System.out.println(bug.getTranslationUnit());
                //System.out.println(bug.getRawAstViolationPiece());
                System.out.println("---------------------------------");
            }
            System.out.println("--------------[END]-------------------");

        } catch (SAXException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SaxonApiException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}
