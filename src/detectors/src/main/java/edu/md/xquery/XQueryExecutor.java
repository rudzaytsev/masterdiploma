package edu.md.xquery;

import net.sf.saxon.s9api.*;

import java.io.File;
import java.io.IOException;

/**
 * Created by rudolph on 31.03.15.
 */
public class XQueryExecutor {

    public static final String TEST_DATA_FILE_PATH = "./testDefects/ex01.xml";
    public static final String TEST_QUERY_FILE_PATH = "./testDefects/query01.xq";

    public static final String DATA_FILE_PATH = "ast.xml";

    public static final String DEFAULT_DOCUMENT_QNAME = "mydoc";


    public static void main(String[] args){
        try {
            XQueryExecutor.runTestXQuery();

        }
        catch(SaxonApiException e){
            System.out.println(e.getMessage());
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    public static void runTestXQuery() throws SaxonApiException, IOException {
        System.out.println("XQuery test");

        XdmValue result = XQueryExecutor.executeQuery(TEST_QUERY_FILE_PATH, TEST_DATA_FILE_PATH);
        System.out.println(result);

    }

    public static XdmValue executeQuery(String queryFileName,String dataFileName) throws SaxonApiException, IOException {
        System.out.println("XQuery method executeQuery");

        Processor processor = new Processor(false);

        DocumentBuilder documentBuilder = processor.newDocumentBuilder();

        File dataFile = new File(dataFileName);
        XdmNode document = documentBuilder.build(dataFile);
        QName name = new QName(DEFAULT_DOCUMENT_QNAME);
        XQueryCompiler xQueryCompiler = processor.newXQueryCompiler();

        File queryFile = new File(queryFileName);
        XQueryExecutable xQueryExecutable = xQueryCompiler.compile(queryFile);

        XQueryEvaluator xQueryEvaluator = xQueryExecutable.load();
        xQueryEvaluator.setExternalVariable(name, document);
        XdmValue result;
        result = xQueryEvaluator.evaluate();

        return result;
    }

}
