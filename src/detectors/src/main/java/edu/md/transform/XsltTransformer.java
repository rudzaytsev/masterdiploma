package edu.md.transform;


import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;

/**
 * Created by rudolph on 24.03.15.
 */
public class XsltTransformer {

    public static final String TRANSFORMATION_FILE_PATH = "./testTransform/transform.xsl";

    public static final String TEST_TRANSFORM_FILES_DIR = "./testTransform/";

    public static final String DEFAULT_METAMODEL_SOURCE_FILE_PATH = "./metamodel.xml";

    public static final String DEFAULT_AST_RESULT_FILE_PATH = "./ast.xml";

    public static void main(String[] args) throws IOException, SAXException, TransformerException, ParserConfigurationException {

       File dir = new File(TEST_TRANSFORM_FILES_DIR);
       if(dir.exists() && dir.isDirectory()){
           String[] sourceFileNames = dir.list(new FilenameFilter() {
               @Override
               public boolean accept(File dir, String name) {

                  return name.matches("^meta[0-9]*.xml$");
               }
           });
           for(String sourceFileName : sourceFileNames){
              String[] number = sourceFileName.split("(meta|.xml)",1);
              StringBuilder builder = new StringBuilder();
              String resultName = builder.append("output")
                                         .append(number[0])
                                         .toString();
              String sourceFilePath = TEST_TRANSFORM_FILES_DIR + sourceFileName;
              String resultFilePath = TEST_TRANSFORM_FILES_DIR + resultName;
              XsltTransformer.transform(sourceFilePath,resultFilePath);
           }

       }

    }

    public static void transform(String sourceFileName, String resultFileName,String transformFileName)
            throws IOException, SAXException, TransformerException, ParserConfigurationException {

        System.out.println("transform : " + sourceFileName + " ==> " + resultFileName );

        File stylesheet = new File(transformFileName);
        StreamSource styleSource = new StreamSource(stylesheet);
        Transformer transformer = TransformerFactory.newInstance().newTransformer(styleSource);

        File datafile = new File(sourceFileName);

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(datafile);


        Result output = new StreamResult(new File(resultFileName));
        Source input = new DOMSource(document);

        transformer.transform(input, output);
    }

    public static void transform(String sourceFileName,String resultFileName)
            throws IOException, SAXException, TransformerException, ParserConfigurationException {

        XsltTransformer.transform( sourceFileName, resultFileName,
                                  XsltTransformer.TRANSFORMATION_FILE_PATH );
    }

    public static void transform() throws SAXException, TransformerException, ParserConfigurationException, IOException {
        XsltTransformer.transform(  DEFAULT_METAMODEL_SOURCE_FILE_PATH,
                                    DEFAULT_AST_RESULT_FILE_PATH,
                                    TRANSFORMATION_FILE_PATH);
    }
}
