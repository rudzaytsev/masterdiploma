


public class CheckForNullBeforeUseRef {

    public static void main(String[] args){

        helloFunc(null);

    }

    public static void helloFunc(String x){

        String a = null;
        String k = null;
        if(x.equals(null)){
            System.out.println("Ruby");
            if( k.equals(null) || 2 != 1  ){
                System.out.println("Erlang");
            }
        }
        else {
            System.out.println("Python");
            if( 1 == 1  && a.equals(null)){
                System.out.println("Haskell");
            }
        }

        if(x == null){
            System.out.println("Java");
        }
    }

}