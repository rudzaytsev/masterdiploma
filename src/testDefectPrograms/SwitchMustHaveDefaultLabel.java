



public class SwitchMustHaveDefaultLabel {

    public static void main(String[] args) {
        test();
    }

    public static void test(){
        int x = 2;
        switch (x){
            case 1: x++; break;
            case 2: x+=2; break;
        }


        int a = 3;
        switch (a){
            case 1: x++; break;
            case 5: x+=3; break;
            default: x+=1; break;
        }
    }

}