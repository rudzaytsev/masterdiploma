import java.lang.Thread;

public class DontCallThreadRun {

    public static void main(String[] args){

        Thread thread = new Thread();
        thread.start();

        Thread t = new Thread();
        t.run();

        new Thread().run();

        thread.join();

    }

}