import java.lang.Runnable;
import java.lang.Thread;
import java.util.Collection;
import java.util.Vector;
import java.lang.Error;

public class MissingStaticMethods {
    private MissingStaticMethods()
    {
        //does nothing
    }

    public void foo(){}
    public void bar(){}


}

class Square {

    private int c;

    private Square(){}
    public static int getOne(){
        return 1;
    }

    @Override
    public int hashCode() {
        return c;
    }

    public static void main(String[] args){

        int i;
        int j;
        int c;
        int sz = 10;
        for( i = 0; i < sz; i++ ){
            System.out.println("Okey!");
        }
        for( i = 0; i < sz; i++ ){
            for( j = 0; j < sz; sz++ ){
                c = i*j;
                System.out.println(c);
            }
        }

        helloFunc(null);


        float st = 2000000000;
        int count = 0;
        for(float f = st; f < st + 50; f++){
            count++;
        }
        System.out.println(count);
        System.out.println(st + 50.0);

    }

    public static void helloFunc(String x){
        String a = null;
        String k = null;
        if(x.equals(null)){
            System.out.println("Ruby");
            if( k.equals(null) || 2 != 1  ){
                System.out.println("Erlang");
            }
        }
    }
}

class Triangle {
    private Triangle(){}
    public Triangle(int a){
        //does nothing

        Thread t = new Thread();
        t.run();
    }
}

class Empty {
    //only default constructor
}


class OtherThread implements Runnable {

    public void someMethod(){
        Runnable t = new Thread();
        t.start();
    }

}

class Box {

    public static void test(){
        int x = 2;
        switch (x){
            case 1: x++; break;
            case 2: x+=2; break;
        }


        int a = 3;
        switch (a){
            case 1: x++; break;
            case 5: x+=3; break;
            default: x+=1; break;
        }
    }

    public static void test2(){

        int a = 20;
        if ( a > 10 ){
            System.out.println("Linux shutdown");
            System.exit(0);

        }
        Collection c = new Vector();
    }

    public static void hello(){
        String a = null;
        String b = "hello";
        if(b.equals(b) || b == "fun"){
            System.out.println("funny");
            if(a.equals(b) && a != null){
                System.out.println(b);
            }
        }

    }
}

public class DontExtendJavaLangError extends Error {

}