

public class CheckForNullBeforeUseRef {

    public static void main(String[] args){

       hello();

    }

    public static void hello(){
        String a = null;
        String b = "hello";
        if(b.equals(b) || b == "fun"){
            System.out.println("funny");
            if(a.equals(b) && a != null){
                System.out.println(b);
            }
        }


        if(b.equals(a) || b != null){
            System.out.println("Nice day!");
        }

        String c = null;

        if(c.equals(b) || null != c){
            System.out.println("Sun!");
        }
    }

}