import java.lang.Object;
import java.lang.Override;
import java.lang.String;

public class OverrideMethodsEqualsAndHashCode {

    int a;

    public void setA(int a) {
        this.a = a;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MethodsEqualsAndHashCode)) return false;

        MethodsEqualsAndHashCode that = (MethodsEqualsAndHashCode) o;

        if (a != that.a) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return a;
    }
}

class Bar {
    int b;

    public int getB() {
        return b;
    }

    public void setB(int b) {
        this.b = b;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Bar)) return false;

        Bar bar = (Bar) o;

        if (b != bar.b) return false;

        return true;
    }

}

class Square {

    private int c;

    @Override
    public int hashCode() {
        return c;
    }
}