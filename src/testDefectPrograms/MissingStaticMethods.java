

public class MissingStaticMethods {
    private MissingStaticMethods(){}

    public void foo(){}
    public void bar(){}
}

class Square {
    private Square(){}
    public static int getOne(){
        return 1;
    }
}

class Triangle {
    private Triangle(){}
    public Triangle(int a){
        //does nothing
    }
}

class Empty {
    //only default constructor
}