
public class AssignmentHasNoEffect {

    int x;

    public void foo(int x){
        x = x;
    }

    public void bar(int a){
        x = a;
    }

    public void hello(){
        System.out.println("Hello");
    }
}