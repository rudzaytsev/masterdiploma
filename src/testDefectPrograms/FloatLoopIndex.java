

public class FloatLoopIndex {

    public static void main(String[] args){

        float st = 2000000000;
        int count = 0;
        for(float f = st; f < st + 50; f++){
            count++;
        }
        System.out.println(count);
        System.out.println(st + 50.0);

        float simple = 3000000000;
        for(int i = 0; i < 10; i++) {
            for (float fIndex = simple; fIndex < simple + 5; fIndex++) {
                count++;
            }
        }
        System.out.println(count);
    }

}