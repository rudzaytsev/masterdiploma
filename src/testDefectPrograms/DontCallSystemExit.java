
public class DontCallSystemExit {

    public static void main(String[] args){

       int a = 20;
       if ( a > 10 ){
           System.out.println("Linux shutdown");
           System.exit(0);

       }
       else {
           System.out.println("Windows");
           if(a < 5 ){
               System.out.println("Win8");
               Runtime.getRuntime().exit(0);
           }
       }

    }

}
