package edu.diploma.parser;

/**
 * Created by rudolph on 19.04.15.
 */
public class LinesCounter {

    public static int line = 1;
    private static LinesCounter instance = null;

    public static String unit = null;   // current Translation Unit

    private LinesCounter(){
        //does nothing
    };

    public synchronized static LinesCounter getInstance(){
        if(instance == null){
            instance = new LinesCounter();
        }
        return instance;
    }


    public void clearAll(){
        line = 1;
        unit = null;
        //does nothing
        //TODO: implement cleaning of all data structures and variables
    }

    public static String getUnit() {
        return unit;
    }

    public static void setUnit(String unit) {
        LinesCounter.unit = unit;
    }
}
