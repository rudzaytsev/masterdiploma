/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.diploma.parser;

import edu.diploma.metamodel.Location;
import edu.diploma.metamodel.declarations.VariableDecl;
import edu.diploma.metamodel.expressions.Expression;
import edu.diploma.metamodel.types.ArrayType;
import edu.diploma.metamodel.types.Type;

/**
 *
 * @author alexander
 */
public class UntypedVariable implements Location {
    private final String name;
    private Expression value;
    private final UntypedVariable next;

    private String transUnit = "UNDEFINED";
    
    private int lineOfCode = 0;
    
    public UntypedVariable(final String name) {
        this.name = name;
        this.next = null;
        this.value = null;
    }
    public UntypedVariable(final String name, final Expression value) {
        this.name = name;
        this.value = value;
        this.next = null;
    }
    public UntypedVariable(final UntypedVariable next) {
        this.name = null;
        this.value = null;
        this.next = next;
    }
    
    public void setValue(final Expression value) {
        this.value = value;
    }
    
    public VariableDecl createVariableDecl(final Type type) {
        if (next != null) {
            return next.createVariableDecl(new ArrayType(type));
        }
        VariableDecl varDecl = new VariableDecl(type, name, value);
        varDecl.setLineOfCode(lineOfCode);
        return varDecl;
    }

    @Override
    public void setLineOfCode(int loc) {
        lineOfCode = loc;
    }

    @Override
    public int getLineOfCode() {
        return lineOfCode;
    }

    @Override
    public String getTransUnit() {
        return transUnit;
    }

    @Override
    public void setTransUnit(String transUnit) {
        this.transUnit = transUnit;
    }
}
